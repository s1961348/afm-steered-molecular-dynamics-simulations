
using namespace std;
#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<fstream>
#include<sstream>

int map[1000][1000];
double value=0;


int main(int argc, char* argv[]) {

//cout << "Input argv: argv[1]:nrows argv[2]:ncolumns argv[3]:mean_intensity argv[4]:stdev_intensity argv[5]:filename"<<endl;
    srand(time(NULL));

    int Nrows = atoi(argv[1]);
    int Ncols = atoi(argv[2]);
    double resolution = atof(argv[3]);
    int nrings = atoi(argv[4]);
    int length_rings_bp = atoi(argv[5]);
    double dsDNAwidth = atof(argv[6]);

    int bins = 3; //low, mid, high = pixel intensity

    ofstream writeW;
    stringstream writeFileW;
    writeFileW << "map.dat";
    writeW.open(writeFileW.str().c_str());

    ofstream writeW1;
    stringstream writeFileW1;
    writeFileW1 << "matrix.dat";
    writeW1.open(writeFileW1.str().c_str());

    double mean_intensity = atof(argv[3]);
    double stdev_intensity = atof(argv[4]);

    ifstream read;
    read.open(argv[7]);
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
    int non = 0;
    int max = 0;

    for (int row = 0; row < Nrows; row++) {
        for (int col = 0; col < Ncols; col++) {
            read >> value;
            if (value == 0)map[row][col] = 0;
            else {
                map[row][col] = value;
                non++;
            }
            //cout << row << " " << col << " " << map[row][col];
            writeW1 << row << " " << col << " " << value << endl;
            if (value > max) max = value;

        }
    }


    writeW << non << endl;

//sets bin size
    int binsize = int((max - mean_intensity) * 1.0 / bins);
//cout << "bs " << binsize <<endl;

    double nm_per_pixel = resolution;
    double sigma_per_nm = 1.0 / dsDNAwidth;
    double factor = nm_per_pixel * sigma_per_nm;
    int nbeads = int(length_rings_bp * 0.34 / dsDNAwidth);
    int npoly = nrings;
    int n = 1;

    for (int row = 0; row < Nrows; row++) {
        for (int col = 0; col < Ncols; col++) {

            int type = 0;
            if (map[row][col] > 0) {
                type = int((map[row][col] - mean_intensity) * 1.0 / binsize);
                cout << "value " << map[row][col] << " -> " << type << endl;

                writeW << nbeads * npoly + n << " " << 1 << " " << 5 + type << " " << row * factor - Nrows * factor / 2.
                       << " " << col * factor - Ncols * factor / 2. << " " << 0 << " " << 0 << " " << 0 << " " << 0
                       << endl;
                n++;
            }

        }
    }

    return 0;
}