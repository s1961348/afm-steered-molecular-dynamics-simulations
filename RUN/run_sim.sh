#! /bin/bash
mpirun="/usr/bin/mpirun"

# Define the number of simulations
num_simulations=100

# Directory where the lammpstrj files are initially saved
dump_dir="./dump"
result_dir="./result"

seed_file="./parameters.dat"

# Loop through each simulation
for i in $(seq 1 $num_simulations)
do
# Generate a new seed for this simulation
# Assuming a random seed between 1 and 100000
    simulation_number=$((i + 100))
    new_seed1=$((1 + $RANDOM % 10000))
    new_seed2=$((1 + $RANDOM % 10000))
    new_seed3=$((1 + $RANDOM % 10000))


# Update the seed in the .dat file
# This command replaces the line containing 'seed = ' with the new seed value
    sed -i "s/variable seed1 equal .*/variable seed1 equal ${new_seed1}/" $seed_file
    sed -i "s/variable seed2 equal .*/variable seed2 equal ${new_seed2}/" $seed_file
    sed -i "s/variable seed3 equal .*/variable seed3 equal ${new_seed3}/" $seed_file

    echo "Simulation ${i}, Seed1: ${new_seed1}" >> seed_record_1.txt
    echo "Simulation ${i}, Seed2: ${new_seed2}" >> seed_record_1.txt
    echo "Simulation ${i}, Seed3: ${new_seed3}" >> seed_record_1.txt

    $mpirun -np 4 lmp_mpi -in run_v3.lam
    
    # Rename and move the lammpstrj file after each simulation
    mv "${dump_dir}/kdna.afm.final.lammpstrj" "${result_dir}/simulation_no4_${i}.lammpstrj"
done
