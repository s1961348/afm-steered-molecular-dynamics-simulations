import numpy as np
import matplotlib.pyplot as plt
import os
from scipy.stats import gaussian_kde


def calculate_angles_from_simulations(simulation_folder, num_simulations):
    angles = []

    for i in range(1, num_simulations + 1):
        #if i == 103 or i == 85 or i == 66:
        #    continue
        filename = os.path.join(simulation_folder, f"simulation_no4_{i}.lammpstrj")

        # Read atoms from file
        atoms = read_lammpstrj(filename)

        # Find the target atom and its two nearest neighbors
        atom2, nearest1, nearest2 = find_nearest_atoms(atoms, target_atom_type, query_atom_type)

        # Calculate the angle
        angle = calculate_angle(atom2, nearest1, nearest2)
        angles.append(angle)

    return angles

def read_lammpstrj(filename):
    with open(filename, 'r') as file:
        lines = file.readlines()

    atoms = []
    for i, line in enumerate(lines):
        if line.startswith("5000000"):
            start = i + 8
            break

    for line in lines[start:]:
        if line.strip():  # Skip empty lines
            parts = line.split()
            atom_id, atom_type, x, y, z = int(parts[0]), int(parts[2]), float(parts[3]), float(parts[4]), float(
                parts[5])
            atoms.append((atom_id, atom_type, np.array([x, y, z])))

    return atoms


def find_nearest_atoms(atoms, target_type, query_type):
    target_atom = None
    query_atoms = []
    for atom in atoms:
        atom_id, atom_type, position = atom
        if atom_type == target_type:
            target_atom = atom
        # This line needs to be modified based on resolutions
        elif atom_id == 17 or atom_id == 42:
            query_atoms.append(atom)

    if not target_atom:
        raise ValueError("No target atom found")

    distances = []
    for atom in query_atoms:
        _, _, position = atom
        distance = np.linalg.norm(position - target_atom[2])
        distances.append((distance, atom))

    distances.sort(key=lambda x: x[0])
    return target_atom, distances[0][1], distances[1][1]


def calculate_angle(atom1, atom2, atom3):
    vector1 = atom2[2] - atom1[2]
    vector2 = atom3[2] - atom1[2]
    cos_angle = np.dot(vector1, vector2) / (np.linalg.norm(vector1) * np.linalg.norm(vector2))
    angle = np.arccos(cos_angle)
    return np.degrees(angle)

def count_nearby_atoms(atoms, target_type, query_types, cutoff_distance):
    """
    Count the number of atoms of specified types (query_types) that are within
    the cutoff_distance from an atom of the target type.
    """
    # Find the target atom (type 2 atom in your case)
    target_atom = next((atom for atom in atoms if atom[1] == target_type), None)
    if not target_atom:
        raise ValueError(f"No target atom of type {target_type} found.")

    # Count the nearby atoms
    nearby_count = 0
    for atom in atoms:
        if atom[1] in query_types:
            distance = np.linalg.norm(atom[2] - target_atom[2])
            if distance <= cutoff_distance:
                nearby_count += 1

    return nearby_count


# Path to lammpstrj file
simulation_folder = r"C:\Users\Admin\Desktop\MPhys\Analysis"
num_simulations = 98

# Find the target atom and its two nearest neighbors
target_atom_type = 2
query_atom_type = 1
angles = calculate_angles_from_simulations(simulation_folder, num_simulations)
print(angles)

# Create a histogram of angles as a probability density
plt.hist(angles, bins=20, alpha=0.6, color='g', edgecolor='black', density=True)

# Calculate the KDE using SciPy
density = gaussian_kde(angles)
xs = np.linspace(0, 180, 500)  # Generate values from 0 to 180 to evaluate the density
kde_values = density(xs)

# Plot the KDE as a smooth line
plt.plot(xs, kde_values, color='blue', lw=2)

peak_x = xs[np.argmax(kde_values)]
peak_y = np.max(kde_values)

# Annotate the peak value on the plot
plt.annotate(f'Peak at: {peak_x:.2f} degrees', xy=(peak_x, peak_y),
             xytext=(peak_x + 10, peak_y), ha='left', va='center',
             fontsize=9, bbox=dict(boxstyle="round,pad=0.3", fc="white", ec="black", lw=1, alpha=0.7))

plt.xlabel('Exit Angle/Degrees')
plt.ylabel('Probability Density')
plt.title('Exit angles between type 1 and type 2 atoms/ending angle')
plt.grid(True)
plt.xlim(0, 180)

plt.show()


#now for the wrapping part

# Assuming the distance cutoff is 1.5 times 1.12 distance units
cutoff_distance = 1.3*1.3*1.12246153
query_types = [1, 3]  # Types 1 and 3 are the query types

# Now calculate the number of nearby atoms for each simulation
nearby_counts = []
for i in range(1, num_simulations + 1):
    #if i == 103 or i == 85 or i == 66:
     #   continue
    filename = os.path.join(simulation_folder, f"simulation_no4_{i}.lammpstrj")
    atoms = read_lammpstrj(filename)
    count = count_nearby_atoms(atoms, target_atom_type, query_types, cutoff_distance)
    nearby_counts.append(count)

# Print or process the nearby_counts as needed:
for i in range(len(nearby_counts)):
    nearby_counts[i] = (nearby_counts[i] * 6)/0.32
print(nearby_counts)

# visualize the distribution of counts
plt.hist(nearby_counts, 20, alpha=0.6, color='r', edgecolor='black',density=True)

# Calculate the KDE using SciPy
density_1 = gaussian_kde(nearby_counts)
xs_1 = np.linspace(0, 280, 50)  # Generate values from 0 to 180 to evaluate the density
kde_values_1 = density_1(xs_1)

# Plot the KDE as a smooth line
plt.plot(xs_1, kde_values_1, color='blue', lw=2)

peak_x_1 = xs_1[np.argmax(kde_values_1)]
peak_y_1 = np.max(kde_values_1)

# Annotate the peak value on the plot
plt.annotate(f'Peak at: {peak_x_1:.2f}', xy=(peak_x_1, peak_y_1),
             xytext=(peak_x_1 + 10, peak_y_1), ha='left', va='center',
             fontsize=9, bbox=dict(boxstyle="round,pad=0.3", fc="white", ec="black", lw=1, alpha=0.7))

plt.xlabel('wrapping length(within 1.4*1.3 sigma to Histone)/bp')
plt.ylabel('Probability density')
plt.title('Distribution of Wrapping length')
plt.grid(True)
plt.show()


data = np.vstack([angles, nearby_counts])
kde = gaussian_kde(data)

# Generate a grid over which to evaluate KDE
xgrid = np.linspace(20, 140, 100)  # Adjusted for angle range
ygrid = np.linspace(100, 230, 200)  # Adjusted for nearby counts range
X, Y = np.meshgrid(xgrid, ygrid)
grid_coords = np.vstack([X.ravel(), Y.ravel()])

# Evaluate KDE over the grid
Z = np.reshape(kde(grid_coords), X.shape)

# Plot setup
fig, ax = plt.subplots(figsize=(8, 6))
ax.set_facecolor('blue')  # Set the axes background color

# Plot the density cloud with contour lines
contourf = ax.contourf(X, Y, Z, levels=50, cmap='gist_heat', alpha=0.75)
fig.colorbar(contourf, ax=ax, label='Probability Density')

# Plot all individual data points
ax.scatter(angles, nearby_counts, s=10, color='y', label='Data Points', alpha=0.5)

# Add grid
ax.grid(color='white', linestyle='-', linewidth=1)  # Customize grid color and style

# Determine contour level for approximately 0.9 probability density (example, adjust as needed)
contour_levels = [np.max(Z) * 0.5]  # Simplified method for demonstration
contour = ax.contour(X, Y, Z, levels=contour_levels, colors='red', linewidths=2)
ax.clabel(contour, inline=True, fontsize=8)

ax.set_xlabel('Exit Angle/Degrees')
ax.set_ylabel('Wrapping length/bp')
ax.set_title('Density Cloud of Exit Angles and Wrapping length with Data Points')
ax.legend()

plt.show()



